import React from 'react'
import { Link, Outlet } from 'react-router-dom'

export const Gibson = () => {
  return (
    <div>
      <h1>Gibson</h1>
      <ul className="flex border-b border-gray-200 text-center">
        <li className="flex-1">
          <Link className="relative block border-t border-l border-r border-gray-200 bg-white p-4 text-sm font-medium" to={'lespaul'}>
            Les Paul
          </Link>
        </li>

        <li className="flex-1">
          <Link className="block p-4 text-sm font-medium text-gray-500" to={'sg'}>
            SG
          </Link>
        </li>
      </ul>

      <Outlet />
    </div>
  )
}
