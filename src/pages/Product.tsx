import { stock } from '@data/stock';
import React, { useMemo } from 'react'
import { useNavigate, useParams } from 'react-router-dom'

export const ProductComponent = () => {
    const navigate = useNavigate();
    let { productId } = useParams();
    const product = useMemo(() => {
        return stock.find(product => product.id === productId);
    }, [productId, stock])

    return (
        <div className='w-3/6 m-auto'>
            <button className='p-2 bg-gray-800 text-white' onClick={()=>{navigate(-1)}}>Regresar</button>

            <a className="block">
                <img
                    alt="Headphones"
                    src="https://images.unsplash.com/photo-1504274066651-8d31a536b11a?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80"
                    className="h-[350px] w-full object-cover sm:h-[450px]"
                />

                <div className="mt-2 space-y-3">

                    <div className="flex flex-col ">
                        <h3>{product?.name}</h3>

                        <p>{product?.price}</p>
                    </div>
                </div>
            </a>

        </div>
    )
}
