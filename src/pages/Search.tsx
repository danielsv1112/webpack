import React, { useState } from 'react'
import { Link, useSearchParams } from 'react-router-dom'
import { stock } from '@data/stock';
import { genericSearch } from '@utilities/genericSearch';


export const Search = () => {
    const [params, setParams] = useSearchParams();

    return (
        <div className='w-3/6 m-auto'>
            <label htmlFor="searchInput" className="block text-md font-medium text-gray-700">
                Buscar:
            </label>

            <input
                type="text"
                id="searchInput"
                placeholder="Buscar por palabra clave"
                value={params.get('q') || ''}
                onChange={(e) => setParams({q: e.target.value})}
                className="my-2 p-2 w-full rounded-md border-2 border-gray-200 shadow-sm sm:text-sm"
            />
            <div className="searchSection">
                {
                stock
                .filter(product => genericSearch(product, ['name'], params.get('q') || ''))
                .map(product => (
                    <p key={product.id}>
                        <Link to={`/product/${product.id}`} relative='route'>{product.name}</Link>
                    </p>
                ))
                }
            </div>


        </div>
    )
}
