import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { HeaderFooterLayout } from '@layouts/HeaderFooterLayout'
import { Gibson } from '@pages/Gibson'
import { Fender } from '@pages/Fender'
import { Schecter } from '@pages/Schecter'
import { Search } from '@pages/Search'
import { ProductComponent } from '@pages/Product'

export const App = () => {
    return (
        <div>
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<HeaderFooterLayout/>}>
                        <Route path='gibson' element={<Gibson/>}>
                            <Route path='lespaul' element={<h1>Les Paul</h1>}/>
                            <Route path='sg' element={<h1>SG</h1>}/>
                        </Route>
                        <Route path='fender' element={<Fender/>}/>
                        <Route path='schecter' element={<Schecter/>}/>
                        <Route path='product'>
                            <Route index element={<h1>Sin selección</h1>}/>
                            <Route path=':productId' element={<ProductComponent/>}/>
                        </Route>
                        <Route path='search' element={<Search/>}/>
                        <Route path='*' element={<h1>404 not found</h1>}/>
                    </Route>
                </Routes>
            </BrowserRouter>            
        </div>
    )
}
