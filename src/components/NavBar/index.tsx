import React from 'react'
import { Link } from 'react-router-dom'

export const NavBar = () => {
    return (
        <nav
            className="hidden gap-8 text-sm font-medium md:flex"
        >
            <Link className="text-gray-500" to={'/'}>Home</Link>
            <Link className="text-gray-500" to={'gibson'}>Gibson</Link>
            <Link className="text-gray-500" to={'fender'}>Fender</Link>
            <Link className="text-gray-500" to={'schecter'}>Schecter</Link>
            <Link className="text-gray-500" to={'search'}>Search</Link>
        </nav>
    )
}
