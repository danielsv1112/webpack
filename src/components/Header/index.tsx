import { NavBar } from '@components/NavBar'
import React from 'react'

export const Header = () => {
    return (
        <header className="shadow-sm">
            <div className="mx-auto max-w-screen-xl p-4">
                <div className="flex items-center justify-between gap-4 lg:gap-10">
                    <div className="flex lg:w-0 lg:flex-1">
                        <a href="#">
                            <span className="sr-only">Logo</span>
                            <span className="h-10 w-20 rounded-lg bg-gray-200"></span>
                        </a>
                    </div>

                    <NavBar/>

                    <div className="flex-1 items-center justify-end gap-4 sm:flex">
                        <a
                            className="rounded-lg bg-gray-100 px-5 py-2 text-sm font-medium text-gray-500"
                            href=""
                        >
                            Log in
                        </a>

                        <a
                            className="rounded-lg bg-blue-600 px-5 py-2 text-sm font-medium text-white"
                            href=""
                        >
                            Sign up
                        </a>
                    </div>

                    <div className="lg:hidden">
                        <button className="rounded-lg bg-gray-100 p-2 text-gray-600">
                            <span className="sr-only">Open menu</span>
                        </button>
                    </div>
                </div>
            </div>
        </header>
    )
}
