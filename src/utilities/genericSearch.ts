export const genericSearch = <T>(obj: T, props: Array<keyof(T)>, query: string): boolean => {
    if(query === ''){
        return false;
    }else{
        return props.some(prop => {
            const value = obj[prop];
            if( typeof(value) === 'string' || typeof(value) === 'number' ){
                return value.toString().toLowerCase().includes(query.toLowerCase());
            }else{
                return false;
            }
        });
    }
}