import webpack from 'webpack';
import webpackCommonConfig, { BUILD_PATH } from './config.common';

interface webpackServerConfig extends webpack.Configuration {
    devServer?: {
        [index: string]: any
    }
}

const webpackDevelopmentConfig = (env: any, arg: any): webpackServerConfig => ({
    ...webpackCommonConfig(env, arg),
    mode: 'development',
    devServer: {
        static: `${BUILD_PATH}`,
        port: 8000,
        open: true,
        historyApiFallback: true,
        liveReload: true,
    }
})

export default webpackDevelopmentConfig;