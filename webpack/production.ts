import webpack from 'webpack';
import TerserWebpackPlugin from 'terser-webpack-plugin';
import webpackCommonConfig from './config.common';

const webpackProductionConfig = (env: any, arg: any): webpack.Configuration => ({
    ...webpackCommonConfig(env, arg),
    mode: 'production',
    optimization: {
        minimize: true,
        minimizer: [
            new TerserWebpackPlugin({
                extractComments: true,
            })
        ]
    }
})

export default webpackProductionConfig;