import path from 'path';
import webpack from 'webpack';
import HTMLWebpackPlugin from 'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

export const BASE_PATH = path.resolve(__dirname, '../');
export const BUILD_PATH = `${BASE_PATH}/build`

const webpackCommonConfig = (env: any, arg: any): webpack.Configuration => ({
    entry: `${BASE_PATH}/index.ts`,
    output: {
        path: BUILD_PATH,
        filename: `index.js`,
        clean: true
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
        alias: {
            '@components': `${BASE_PATH}/src/components/`,
            '@layouts': `${BASE_PATH}/src/layouts/`,
            '@pages': `${BASE_PATH}/src/pages/`,
            '@data': `${BASE_PATH}/src/data/`,
            '@utilities': `${BASE_PATH}/src/utilities/`
        },
    },
    module: {
        rules: [
            {
                test: [ /\.tsx?$/, /\.jsx?$/, /\.js?$/, /\.ts?$/ ],
                use: 'babel-loader',
                exclude: '/node_modules'
            },
            {
                test: [ /\.html?$/ ],
                use: 'html-loader',
                exclude: '/node_modules'
            },
            // {
            //     test: [ /\.css?$/, /\.scss?$/ ],
            //     use: [ MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader' ],
            //     exclude: '/node-modules'
            // },
            {
                test: /\.css$/,
                use: [ "style-loader", { loader: "css-loader", options: { importLoaders: 1 } }, "postcss-loader",],
                exclude: '/node_modules'
            }
        ]
    },
    plugins: [
        new HTMLWebpackPlugin({
            filename: './index.html',
            template: `${BASE_PATH}/public/index.html`,
            inject: true
        }),
        new MiniCssExtractPlugin({
            filename: './style.css'
        })
    ]
})

export default webpackCommonConfig;